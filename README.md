<div align="center">
<img src="static/logo.png">
<h1>Vidzy</h1>
A free and open source alternative to TikTok

<a href="https://vidzy.codeberg.page/">Website</a>
&nbsp;•&nbsp;
<a href="https://matrix.to/#/#vidzysocial:fedora.im">Matrix</a>

![License: AGPL-v3.0](./license_badge.svg)
</div>

<br><br>

## Install

	git clone https://codeberg.org/vidzy/vidzy
	cd vidzy
	pip install -r requirements.txt
	python -m waitress --port=8080 --call app:create_app

## Screenshots

### Homepage
![Homepage Screenshot](./screenshots/homepage.png)

## Features

We have very ALPHA federation through [ActivityPub](https://www.w3.org/TR/activitypub/).

## Contributing

Thank you for considering contributing to Vidzy! To contribute, fork the repo and add your contribution. Then, send a pull request and if it is helpful we will gladly accept it.

## Security Vulnerabilities

If you discover a security vulnerability within Vidzy, please send an e-mail to me via [vidzy_social@proton.me](mailto:vidzy_social@proton.me). All security vulnerabilities will be promptly addressed.

## License

Vidzy is open-source software licensed under the GNU Affero General Public License v3.0